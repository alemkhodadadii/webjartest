﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Upload.API.Data;

namespace Upload.API.Repositories
{
    public interface IUploadRepository
    {
        string UploadedFile(IFormFile file);
        string NameGenerator();
    }

    public class UploadRepository: IUploadRepository
    {
        private readonly UploadContext context;
        private readonly IWebHostEnvironment environment;
        public UploadRepository(UploadContext context, IWebHostEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
        }

        public string UploadedFile(IFormFile file)
        {
            string uniqueFileName = null;
            if (file != null)
            {
                string uploadsFolder = System.IO.Path.Combine(environment.ContentRootPath, "Upload");
                string type = file.FileName.Split('.')[file.FileName.Split('.').Count() - 1];
                uniqueFileName = NameGenerator() + "." + type;
                string filePath = System.IO.Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        public string NameGenerator()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var finalString = new String(stringChars);
            return finalString;
        }


        //public string CalculatePrice(Product product)
        //{
        //    int totalPrice = Int32.Parse(product.Price);

        //    if(product.Attributes.Count() > 0)
        //    {
        //        for (int i = 0; i < product.Attributes.Count; i++)
        //        {
        //            var attrs = product.Attributes[i].Attribute;
        //            if(attrs.Options.Count() > 0)
        //            {
        //                for (int j = 0; j < attrs.Options.Count(); j++)
        //                {
        //                    var opt = attrs.Options[j];
        //                    if (opt.AdjustmentEffect == AdjustmentEffect.Constant)
        //                        totalPrice += opt.PriceAdjustment;
        //                    else
        //                        totalPrice += totalPrice * opt.PriceAdjustment;                             
        //                }
        //            }
        //        }
        //    }
        //    return totalPrice.ToString();
        //}

    }
}
