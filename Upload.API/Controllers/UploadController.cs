﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Upload.API.Repositories;

namespace Comment.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IUploadRepository uploadRepository;
        public UploadController(IUploadRepository uploadRepository)
        {
            this.uploadRepository = uploadRepository;
        }

        [HttpPost]
        public IActionResult AddImage(IFormFile Image)
        {
            try
            {
                if (Image == null)
                    return BadRequest();

                return Ok(uploadRepository.UploadedFile(Image));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in adding image" + e.Message);
            }
        }
    }
}
