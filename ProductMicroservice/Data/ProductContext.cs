﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductMicroservice.Models;

namespace ProductMicroservice.Data
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
            :base(options)
        {
        } 

        public DbSet<Product> Products { get; set; }
        public DbSet<OrderedProduct> OrderedProducts { get; set; }
        public DbSet<AttributeOption> Options { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductTag> Tags { get; set; }
        public DbSet<Product_ProductAttribute> Product_ProductAttributes { get; set; }
        public DbSet<ProductAttribute> Attributes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category()
                {
                    Id = Guid.NewGuid(),
                    Title = "Electronics"
                },
                new Category()
                {
                    Id = Guid.NewGuid(),
                    Title = "clothes"
                },
                new Category()
                {
                    Id = Guid.NewGuid(),
                    Title = "health"
                },
                new Category()
                {
                    Id = Guid.NewGuid(),
                    Title = "furniture"
                }
            );

            modelBuilder.Entity<ProductAttribute>().HasData(
                new ProductAttribute()
                {
                    Id = Guid.NewGuid(),
                    Title = "Size",
                    IsRequired = true
                },
                new ProductAttribute()
                {
                    Id = Guid.NewGuid(),
                    Title = "Color",
                    IsRequired = true
                },
                new ProductAttribute()
                {
                    Id = Guid.NewGuid(),
                    Title = "Gender",
                    IsRequired = true
                },
                new ProductAttribute()
                {
                    Id = Guid.NewGuid(),
                    Title = "Quality",
                    IsRequired = true
                }
            );
        }
    }
}
