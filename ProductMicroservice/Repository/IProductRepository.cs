﻿using ProductMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductMicroservice.Repositories
{
    public interface IProductRepository
    {
        //crud methods for product 
        void Add(Product product);
        void Add(AttributeOption option);
        void Add(Category option);
        void Add(ProductAttribute attribute);
        void Update(Product product);
        void Update(AttributeOption option);
        void Update(Category product);
        void Update(ProductAttribute option);
        void RemoveProduct(Guid productId);
        void RemoveOption(Guid optionId);
        void RemoveCategory(Guid categoryId);
        void RemoveAttribute(Guid attributeId);
        Product GetProductById(Guid productId);
        AttributeOption GetAttributeOptionById(Guid productId);
        Category GetCategoryById(Guid productId);
        ProductAttribute GetProductAttributeById(Guid productId);
        IEnumerable<Product> GetProducts();
        IEnumerable<AttributeOption> GetAttributeOptions();
        IEnumerable<Category> GetCategories();
        IEnumerable<ProductAttribute> GetProductAttributes();
    }
}
