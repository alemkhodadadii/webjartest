﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ProductMicroservice.Data;
using ProductMicroservice.Models;

namespace ProductMicroservice.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext context;
        public ProductRepository(ProductContext context)
        {
            this.context = context;
        }

        public void Add(Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();
        }

        public void Add(AttributeOption attrOption)
        {
            context.Options.Add(attrOption);
            context.SaveChanges();
        }

        public void Add(Category category)
        {
            context.Categories.Add(category);
            context.SaveChanges();
        }

        public void Add(ProductAttribute productAttr)
        {
            context.Attributes.Add(productAttr);
            context.SaveChanges();
        }

        public void Update(Product product)
        {
            var result = context.Products.FirstOrDefault(x => x.Id == product.Id);
            
            result.Title = product.Title;
            result.Code = product.Code;
            result.CategoryId = product.CategoryId;
            result.Img = product.Img;
            result.Price = product.Price;

            context.SaveChanges();
            
        }

        public void Update(AttributeOption attrOption)
        {
            var result = context.Options.FirstOrDefault(x => x.Id == attrOption.Id);

            result.Title = attrOption.Title;
            result.PriceAdjustment = attrOption.PriceAdjustment;
            result.AdjustmentEffect = attrOption.AdjustmentEffect;
            result.ProductAttributeId = attrOption.ProductAttributeId;

            context.SaveChanges();
        }

        public void Update(Category category)
        {
            var result = context.Categories.FirstOrDefault(x => x.Id == category.Id);

            result.Title = category.Title;
            
            context.SaveChanges();

        }

        public void Update(ProductAttribute productAttr)
        {
            var result = context.Attributes.FirstOrDefault(x => x.Id == productAttr.Id);

            result.Title = productAttr.Title;
            result.IsRequired = productAttr.IsRequired;
            
            context.SaveChanges();

        }

        public void RemoveProduct(Guid productId)
        {
            var product = context.Products.FirstOrDefault(x => x.Id == productId);
            if (product != null)
            {
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }

        public void RemoveOption(Guid optId)
        {
            var option = context.Options.FirstOrDefault(x => x.Id == optId);
            if (option != null)
            {
                context.Options.Remove(option);
                context.SaveChanges();
            }
        }

        public void RemoveCategory(Guid catId)
        {
            var category = context.Categories.FirstOrDefault(x => x.Id == catId);
            if (category != null)
            {
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }

        public void RemoveAttribute(Guid attrId)
        {
            var attribute = context.Attributes.FirstOrDefault(x => x.Id == attrId);
            if (attribute != null)
            {
                context.Attributes.Remove(attribute);
                context.SaveChanges();
            }
        }

        public Product GetProductById(Guid productId)
        {
            var product = context.Products
                .Include(x => x.Attributes)
                .FirstOrDefault(x => x.Id == productId);
            return product;
        }

        public AttributeOption GetAttributeOptionById(Guid optId)
        {
            var option = context.Options
                .FirstOrDefault(x => x.Id == optId);
            return option;
        }

        public Category GetCategoryById(Guid catId)
        {
            var category = context.Categories
                .FirstOrDefault(x => x.Id == catId);
            return category;
        }

        public ProductAttribute GetProductAttributeById(Guid attrId)
        {
            var attribute = context.Attributes
                .FirstOrDefault(x => x.Id == attrId);
            return attribute;
        }

        public IEnumerable<Product> GetProducts()
        {
            return context.Products
                .Include(x => x.Attributes)
                .ToList();
        }

        public IEnumerable<AttributeOption> GetAttributeOptions()
        {
            return context.Options
                .ToList();
        }

        public IEnumerable<Category> GetCategories()
        {
            return context.Categories
                .ToList();
        }

        public IEnumerable<ProductAttribute> GetProductAttributes()
        {
            return context.Attributes
                .ToList();
        }
    }
}
