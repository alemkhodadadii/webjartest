﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMicroservice.Repositories;
using ProductMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductMicroservice.Areas.Products.Controllers
{
    [Area("Products")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public CategoryController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult Get()
        {
            var categories = _productRepository.GetCategories();
            return new OkObjectResult(categories);
        }

        // GET api/<ProductController>/Guid
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var result = _productRepository.GetCategoryById(id);
                if (result == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error retrieving category data from database" + e.Message);
            }
        }

        // POST api/<ProductController>
        [HttpPost("add")]
        public IActionResult CreateCategory(Category category)
        {
            try
            {
                if (category == null)
                    return BadRequest();

                _productRepository.Add(category);
                return CreatedAtAction(nameof(Get), new { id = category.Id }, category);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in creating category" + e.Message);
            }
        }

        // PUT api/<ProductController>/Guid
        [HttpPut("update/{id:Guid}")]
        public IActionResult UpdateCategory([FromRoute] Guid id, [FromBody] Category category)
        {
            try
            {
                var categoryToBeUpdated = _productRepository.GetCategoryById(id);

                if (categoryToBeUpdated == null)
                    return NotFound();

                _productRepository.Update(category);
                return Ok(categoryToBeUpdated);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in updating category" + e.Message);
            }
        }
    }
}
