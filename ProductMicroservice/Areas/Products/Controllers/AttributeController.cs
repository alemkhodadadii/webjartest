﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMicroservice.Repositories;
using ProductMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductMicroservice.Areas.Products.Controllers
{
    [Area("Products")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public AttributeController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult Get()
        {
            var attributes = _productRepository.GetProductAttributes();
            return new OkObjectResult(attributes);
        }

        // GET api/<ProductController>/Guid
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var result = _productRepository.GetProductAttributeById(id);
                if (result == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error retrieving product attribute data from database" + e.Message);
            }
        }

        // POST api/<ProductController>
        [HttpPost("add")]
        public IActionResult CreateAttribute(ProductAttribute attribute)
        {
            try
            {
                if (attribute == null)
                    return BadRequest();

                _productRepository.Add(attribute);
                return CreatedAtAction(nameof(Get), new { id = attribute.Id }, attribute);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in creating attribute" + e.Message);
            }
        }

        // PUT api/<ProductController>/Guid
        [HttpPut("update/{id:Guid}")]
        public IActionResult UpdateProduct([FromRoute] Guid id, [FromBody] ProductAttribute attribute)
        {
            try
            {
                var attributeToBeUpdated = _productRepository.GetProductAttributeById(id);

                if (attributeToBeUpdated == null)
                    return NotFound();

                _productRepository.Update(attribute);
                return Ok(attributeToBeUpdated);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in updating attribute" + e.Message);
            }
        }
    }
}
