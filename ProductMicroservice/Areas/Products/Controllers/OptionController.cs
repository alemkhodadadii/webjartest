﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMicroservice.Repositories;
using ProductMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductMicroservice.Areas.Products.Controllers
{
    [Area("Products")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class OptionController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public OptionController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult Get()
        {
            var options = _productRepository.GetAttributeOptions();
            return new OkObjectResult(options);
        }

        // GET api/<ProductController>/Guid
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var result = _productRepository.GetAttributeOptionById(id);
                if (result == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error retrieving attribute option data from database" + e.Message);
            }
        }

        // POST api/<ProductController>
        [HttpPost("add")]
        public IActionResult CreateAttributeOption(AttributeOption option)
        {
            try
            {
                if (option == null)
                    return BadRequest();

                _productRepository.Add(option);
                return CreatedAtAction(nameof(Get), new { id = option.Id }, option);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in creating attribute option" + e.Message);
            }
        }

        // PUT api/<ProductController>/Guid
        [HttpPut("update/{id:Guid}")]
        public IActionResult UpdateAttributeOption([FromRoute] Guid id, [FromBody] AttributeOption option)
        {
            try
            {
                var optionToBeUpdated = _productRepository.GetAttributeOptionById(id);

                if (optionToBeUpdated == null)
                    return NotFound();

                _productRepository.Update(option);
                return Ok(optionToBeUpdated);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in updating attribute option" + e.Message);
            }
        }
    }
}
