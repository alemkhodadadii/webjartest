﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMicroservice.Repositories;
using ProductMicroservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductMicroservice.Areas.Products.Controllers
{
    [Area("Products")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public IActionResult Get()
        {
            var products = _productRepository.GetProducts();
            return new OkObjectResult(products);
        }

        // GET api/<ProductController>/Guid
        [HttpGet("{id:Guid}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var result = _productRepository.GetProductById(id);
                if (result == null)
                {
                    return NotFound();
                }
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error retrieving product data from database" + e.Message);
            }
        }

        // POST api/<ProductController>
        [HttpPost]
        public IActionResult CreateProduct([FromBody] Product product)
        {
            try
            {
                if (product == null)
                    return BadRequest();

                _productRepository.Add(product);
                return CreatedAtAction(nameof(Get), new { id = product.Id }, product);  
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in creating product" + e.Message);
            }
        }

        // PUT api/<ProductController>/Guid
        [HttpPut("update/{id:Guid}")]
        public IActionResult UpdateProduct([FromRoute] Guid id, [FromBody] Product product)
        {
            try
            {
                var productToBeUpdated = _productRepository.GetProductById(id);

                if (productToBeUpdated == null)
                    return NotFound();

                _productRepository.Update(product);
                return Ok(productToBeUpdated);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in updating product" + e.Message);
            }
        }
    }
}
