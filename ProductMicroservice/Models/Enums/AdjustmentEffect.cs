﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ProductMicroservice.Models.Enums
{
    public enum AdjustmentEffect
    {
        [EnumMember(Value = "percentage")]
        Percentage = 0,
        [EnumMember(Value = "constant")]
        Constant = 1
    }
}
