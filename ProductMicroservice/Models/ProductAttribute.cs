﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ProductMicroservice.Models
{
    public class ProductAttribute : BaseClass
    {
        public string Title { get; set; }
        public bool IsRequired { get; set; }
        public virtual List<Product_ProductAttribute> Products { get; set; }
        public List<AttributeOption> Options { get; set; }
    }
}
