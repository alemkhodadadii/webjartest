﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductMicroservice.Models.Enums;

namespace ProductMicroservice.Models
{
    public class AttributeOption : BaseClass
    {
        public string Title { get; set; }
        public int PriceAdjustment { get; set; }
        public AdjustmentEffect AdjustmentEffect { get; set; }
        public Guid ProductAttributeId { get; set; }
    }
}
