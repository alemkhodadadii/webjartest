﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ProductMicroservice.Models
{
    public class Product_ProductAttribute : BaseClass
    {
        public Guid ProductId { get; set; }
        public Guid ProductAttributeId { get; set; }
        [ForeignKey("ProductAttributeId")]
        public virtual ProductAttribute Attribute { get; set; }
    }
}
