﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProductMicroservice.Models
{
    public class OrderedProduct : BaseClass
    {
        public int Quantity { get; set; }
        [ForeignKey("ProductID")]
        public Product Product { get; set; }
        public Guid ProductID { get; set; }
        public List<AttributeOption> Options { get; set; }
    }
}
