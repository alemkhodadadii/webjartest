﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ProductMicroservice.Models
{
    public class Product : BaseClass
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public string Img { get; set; }
        public string Price { get; set; }
        public Guid CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public virtual List<Product_ProductAttribute> Attributes { get; set; }
        public List<ProductTag> Tags { get; set; }
    }
}
