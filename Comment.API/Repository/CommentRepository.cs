﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Comment.API.Data;
using Comment.API.Models;
using MongoDB.Driver;
using Comment.API.Configurations;
using Microsoft.Extensions.Options;

namespace Comment.API.Repositories
{
    public interface ICommentRepository
    {
        IEnumerable<Comment.API.Models.Comment> GetComments(Guid productId);
        Comment.API.Models.Comment GetComment(Guid commentId);
        void AddComment(Comment.API.Models.Comment comment);
        void EditComment(Comment.API.Models.Comment comment);
        void DeleteComment(Guid commentId);
    }

    public class CommentRepository: ICommentRepository
    {
        private readonly IMongoCollection<Comment.API.Models.Comment> _comment;
        private readonly DevDatabaseConfiguration _settings;
        private readonly IWebHostEnvironment environment;
        public CommentRepository(IWebHostEnvironment environment, IOptions<DevDatabaseConfiguration> settings)
        {
            _settings = settings.Value; 
            this.environment = environment;
            var client = new MongoClient(_settings.ConnectionString);
            var database = client.GetDatabase(_settings.DatabaseName);
            _comment = database.GetCollection<Comment.API.Models.Comment>(_settings.CommentCollectionName);
        }

        public IEnumerable<Comment.API.Models.Comment> GetComments(Guid productId)
        {
            var comments = _comment.Find(x => true).ToList();
            return comments;
        }

        public Comment.API.Models.Comment GetComment(Guid commentId)
        {
            var comment =_comment.Find(x=>x.Id == commentId).First();
            return comment;
        }

        public async void AddComment(Comment.API.Models.Comment comment)
        {
            await _comment.InsertOneAsync(comment);
        }

        public void EditComment(Comment.API.Models.Comment comment)
        {
            _comment.ReplaceOne(x=>x.Id == comment.Id, comment);
        }

        public void DeleteComment(Guid commentId)
        {
            _comment.DeleteOne(x => x.Id == commentId);
        }
    }
}
