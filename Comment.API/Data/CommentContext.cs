﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Comment.API.Models;

namespace Comment.API.Data
{
    public class CommentContext : DbContext
    {
        public CommentContext(DbContextOptions<CommentContext> options)
            :base(options)
        {
        } 

        public DbSet<Comment.API.Models.Comment> Comments { get; set; }
       
    }
}
