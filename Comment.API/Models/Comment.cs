﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Comment.API.Models
{
    public class Comment
    {
        [BsonId]
        public Guid Id { get; set; }
        public Guid ProductID {get;set;}
        public string Text { get; set; }
        public string PersonName { get; set; }
    }
}
