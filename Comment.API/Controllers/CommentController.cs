﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Comment.API.Models;
using Comment.API.Repositories;

namespace Comment.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentRepository commentRepository;
        public CommentController(ICommentRepository commentRepository)
        {
            this.commentRepository = commentRepository;
        }

        [HttpGet("all/{id:Guid}")]
        public IActionResult GetComments([FromRoute] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return NotFound();

                var comments = commentRepository.GetComments(id);

                return new OkObjectResult(comments);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in adding comment" + e.Message);
            }
        }

        [HttpGet("{id:Guid}")]
        public IActionResult GetComment([FromRoute] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return BadRequest();

                var comments = commentRepository.GetComment(id);

                return new OkObjectResult(comments);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in adding comment" + e.Message);
            }
        }

        [HttpPost]
        public IActionResult AddComment([FromBody] Comment.API.Models.Comment comment)
        {
            try
            {
                if (comment == null)
                    return BadRequest();

                commentRepository.AddComment(comment);

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in adding comment" + e.Message);
            }
        }

        [HttpPut]
        public IActionResult UpdateComment (Comment.API.Models.Comment comment)
        {
            try
            {
                if (comment == null)
                    return BadRequest();

                if (commentRepository.GetComment(comment.Id) == null)
                    return NotFound();

                commentRepository.EditComment(comment);

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in updating comment" + e.Message);
            }
        }


        [HttpDelete("{id:Guid}")]
        public IActionResult RemoveComment([FromRoute] Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return BadRequest();

                commentRepository.DeleteComment(id);

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "error in deleting comment" + e.Message);
            }
        }
    }
}
